﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ioasys_filmes_jrp_api.Models;

    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbContext (DbContextOptions<DbContext> options)
            : base(options)
        {
        }

        public DbSet<ioasys_filmes_jrp_api.Models.User> Users { get; set; }

        public DbSet<ioasys_filmes_jrp_api.Models.Gender> Genders { get; set; }

        public DbSet<ioasys_filmes_jrp_api.Models.Movie> Movies { get; set; }

        public DbSet<ioasys_filmes_jrp_api.Models.Rating> Ratings { get; set; }
    }
