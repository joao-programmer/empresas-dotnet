﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace movies_jpr_api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Genders",
                columns: table => new
                {
                    GenderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GenderDescription = table.Column<string>(type: "varchar(100)", nullable: false),
                    GenderCreateUserId = table.Column<int>(type: "int", nullable: false),
                    GenderUpdateUserId = table.Column<int>(type: "int", nullable: true),
                    GenderCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    GenderUpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genders", x => x.GenderId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(type: "varchar(100)", nullable: false),
                    UserPassword = table.Column<string>(type: "varchar(20)", nullable: false),
                    UserCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserUpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UserInactivationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UserRole = table.Column<int>(type: "int", nullable: false),
                    UserStatus = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieName = table.Column<string>(type: "varchar(100)", nullable: false),
                    MovieSynopsis = table.Column<string>(type: "varchar(1000)", nullable: true),
                    MovieDirector = table.Column<string>(type: "varchar(100)", nullable: true),
                    MovieActors = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MovieCreateUserId = table.Column<int>(type: "int", nullable: false),
                    MovieUpdateUserId = table.Column<int>(type: "int", nullable: true),
                    MovieGenderIdFk = table.Column<int>(type: "int", nullable: false),
                    MovieCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MovieUpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    MovieImagePath = table.Column<string>(type: "varchar(1000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movies_Genders_MovieGenderIdFk",
                        column: x => x.MovieGenderIdFk,
                        principalTable: "Genders",
                        principalColumn: "GenderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ratings",
                columns: table => new
                {
                    RatingId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RatingStar = table.Column<short>(type: "smallint", nullable: false),
                    RatingComment = table.Column<string>(type: "varchar(500)", nullable: true),
                    RatingUserIdFk = table.Column<int>(type: "int", nullable: false),
                    RatingMovieIdFk = table.Column<int>(type: "int", nullable: false),
                    RatingCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RatingUpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ratings", x => x.RatingId);
                    table.ForeignKey(
                        name: "FK_Ratings_Movies_RatingMovieIdFk",
                        column: x => x.RatingMovieIdFk,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Ratings_Users_RatingUserIdFk",
                        column: x => x.RatingUserIdFk,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movies_MovieGenderIdFk",
                table: "Movies",
                column: "MovieGenderIdFk");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_RatingMovieIdFk",
                table: "Ratings",
                column: "RatingMovieIdFk");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_RatingUserIdFk",
                table: "Ratings",
                column: "RatingUserIdFk");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ratings");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Genders");
        }
    }
}
