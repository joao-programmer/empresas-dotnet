﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ioasys_filmes_jrp_api.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string UserName { get; set; }

        [Column(TypeName = "varchar(20)")]
        public string UserPassword { get; set; }

        public DateTime UserCreateDate { get; set; }

        public DateTime? UserUpdateDate { get; set; }

        public DateTime? UserInactivationDate { get; set; }

        public UserRole UserRole { get; set; }

        public UserStatus UserStatus { get; set; }
    }

    public enum UserRole { Admin, User }

    public enum UserStatus { Active, Inactive}
}
