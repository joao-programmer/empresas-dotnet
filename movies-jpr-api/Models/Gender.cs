﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ioasys_filmes_jrp_api.Models
{
    public class Gender
    {
        [Key]
        public int GenderId { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string GenderDescription { get; set; }

        public int GenderCreateUserId { get; set; }

        public int? GenderUpdateUserId { get; set; }

        public DateTime GenderCreateDate { get; set; }

        public DateTime? GenderUpdateDate { get; set; }

        public IList<Movie> GenderMovieList { get; set; } = new List<Movie>();
    }
}
