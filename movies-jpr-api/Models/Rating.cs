﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ioasys_filmes_jrp_api.Models
{
    public class Rating // Classificação dos filmes por usuários
    {
        [Key]
        public long RatingId { get; set; }

        public short RatingStar { get; set; } // Voto

        [Column(TypeName = "varchar(500)")]
        public string? RatingComment { get; set; }

        [ForeignKey("RatingUser")]
        public int RatingUserIdFk { get; set; }

        public User? RatingUser { get; set; }

        [ForeignKey("RatingMovie")]
        public int RatingMovieIdFk { get; set; }

        public Movie? RatingMovie { get; set; }

        public DateTime RatingCreateDate { get; set; }

        public DateTime? RatingUpdateDate { get; set; }
    }
}
