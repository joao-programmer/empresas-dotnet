﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ioasys_filmes_jrp_api.Models
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string MovieName { get; set; }

        [Column(TypeName = "varchar(1000)")]
        public string? MovieSynopsis { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string? MovieDirector { get; set; }

        public string? MovieActors { get; set; }

        public int MovieCreateUserId { get; set; }
        public int? MovieUpdateUserId { get; set; }

        [ForeignKey("MovieGender")]
        public int MovieGenderIdFk { get; set; }


        public Gender? MovieGender { get; set; }

        public DateTime MovieCreateDate { get; set; }

        public DateTime? MovieUpdateDate { get; set; }

        [Column(TypeName = "varchar(1000)")]
        public string? MovieImagePath { get; set; }

        [NotMapped]
        public IFormFile? MovieImageFile { get; set; }

        [NotMapped]
        public int? countRating { get; set; }

        [NotMapped]
        public int? countStar { get; set; }

        [NotMapped]
        public decimal? avarageStar { get; set; }

    }
}
