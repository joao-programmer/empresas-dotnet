﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ioasys_filmes_jrp_api.Models;

namespace movies_jpr_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingsController : ControllerBase
    {
        private readonly DbContext _context;

        public RatingsController(DbContext context)
        {
            _context = context;
        }

        // GET: api/Ratings
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Rating>>> GetRating()
        {
            return await _context.Ratings.ToListAsync();
        }

        // GET: api/Ratings/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Rating>> GetRating(long id)
        {
            var rating = await _context.Ratings.FindAsync(id);

            if (rating == null)
            {
                return NotFound();
            }

            return rating;
        }

        // PUT: api/Ratings/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRating(long id, Rating rating)
        {
            User user = await _context.Users.FindAsync(rating.RatingUserIdFk);

            Rating originalRating = await _context.Ratings.FindAsync(id);

            if (user == null)
            {
                return BadRequest("Usuário não localizado!");
            }

            if (user.UserRole == UserRole.Admin)
            {
                return BadRequest("Administradores não podem votar!");
            }

            if (rating.RatingStar <= 0 || rating.RatingStar > 4)
            {
                return BadRequest("Favor informar uma avaliação de 0 a 4 estrelas!");
            }

            if (user.UserId != rating.RatingUserIdFk)
            {
                return BadRequest("Um usuário não pode alterar a avaliação de outro usuário!");
            }

            rating.RatingUpdateDate = DateTime.Now;

            if (id != rating.RatingId)
            {
                return BadRequest();
            }

            _context.Entry(rating).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RatingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ratings
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Rating>> PostRating(Rating rating)
        {
            User user = await _context.Users.FindAsync(rating.RatingUserIdFk);

            if (user == null)
            {
                return BadRequest("Usuário não localizado!");
            }

            if (user.UserRole == UserRole.Admin)
            {
                return BadRequest("Administradores não podem votar!");
            }

            if(rating.RatingStar < 0 || rating.RatingStar > 4)
            {
                return BadRequest("Favor informar uma avaliação de 0 a 4 estrelas!");
            }

            rating.RatingCreateDate = DateTime.Now;
            rating.RatingUpdateDate = null;

            _context.Ratings.Add(rating);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRating", new { id = rating.RatingId }, rating);
        }

        // DELETE: api/Ratings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRating(long id)
        {
            var rating = await _context.Ratings.FindAsync(id);
            if (rating == null)
            {
                return NotFound();
            }

            _context.Ratings.Remove(rating);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool RatingExists(long id)
        {
            return _context.Ratings.Any(e => e.RatingId == id);
        }
    }
}
