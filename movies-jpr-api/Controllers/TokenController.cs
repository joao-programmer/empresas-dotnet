﻿using ioasys_filmes_jrp_api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movies_jpr_api.Services;

namespace movies_jpr_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly DbContext _context;

        public TokenController(DbContext context)
        {
            _context = context;
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody]User model)
        {
            var user = await (from users in _context.Set<User>() 
                               where users.UserName == model.UserName 
                               && users.UserPassword == model.UserPassword 
                               select users)
                               .FirstOrDefaultAsync();

            if (user == null)
            {
                return NotFound(new { message = "Usuário ou senha inválidos" });
            }

            var token = TokenService.GenerateToken(user);
            user.UserPassword = "";

            return new
            {
                user = user,
                token = token
            };
        }
        
    }
}
