﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ioasys_filmes_jrp_api.Models;

namespace movies_jpr_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly DbContext _context;

        public MoviesController(DbContext context)
        {
            _context = context;
        }

        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Movie>>> GetMovie()
        {
            var movies = await _context.Movies.ToListAsync();

            foreach (Movie movie in movies)
            {
                movie.countRating = (from ratings in _context.Ratings where movie.MovieId == ratings.RatingMovieIdFk select ratings).Count();
                movie.countStar = (from ratings in _context.Ratings where movie.MovieId == ratings.RatingMovieIdFk select ratings).Sum(r => r.RatingStar);
                movie.avarageStar = movie.countRating > 0 ? Math.Round((decimal)movie.countStar / (decimal)movie.countRating, 2) : 0;
            }

            return movies;
        }

        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Movie>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            movie.countRating = (from ratings in _context.Ratings where movie.MovieId == ratings.RatingMovieIdFk select ratings).Count();
            movie.countStar = (from ratings in _context.Ratings where movie.MovieId == ratings.RatingMovieIdFk select ratings).Sum(r => r.RatingStar);
            movie.avarageStar = movie.countRating > 0 ? Math.Round( (decimal) movie.countStar / (decimal) movie.countRating ,2) : 0;

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }

        // GET: api/Movies/PesquisarFilmes
        [HttpGet("/PesquisarFilmes")]
        public async Task<ActionResult<IEnumerable<Movie>>> PesquisarFilmes(int pageNumber, int pageSize, string director, string name, string actors, string gender)
        {
            if (pageNumber < 1) pageNumber = 1;
            if (pageSize < 10) pageSize = 10; // Mínimo 10 users por página

            var movie = await (from movies in _context.Movies
                               where
                                   (string.IsNullOrWhiteSpace(director) || movies.MovieDirector.Contains(director))
                                   && (string.IsNullOrWhiteSpace(name) || movies.MovieName.Contains(name))
                                   && (string.IsNullOrWhiteSpace(actors) || movies.MovieActors.Contains(actors))
                                   && (string.IsNullOrWhiteSpace(gender) || movies.MovieGender.GenderDescription.Contains(gender))
                               select new { 
                                   id = movies.MovieId,
                                   name = movies.MovieName,
                                   director = movies.MovieDirector,
                                   actors = movies.MovieActors,
                                   gender = movies.MovieGender.GenderDescription,
                                   countStars = (from ratings in _context.Ratings where movies.MovieId == ratings.RatingMovieIdFk select ratings).Sum(r => r.RatingStar)
                               }
                               )
                               .OrderBy(m => m.countStars)
                               .OrderBy(m => m.name)
                               .Skip((pageNumber - 1) * pageSize)
                               .Take(pageSize)
                               .ToListAsync();

            if (movie == null)
            {
                return NotFound("Nenhum filme encontrado!");
            }

            return Ok(movie);
        }

        // PUT: api/Movies/5        
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            User user = await _context.Users.FindAsync(movie.MovieUpdateUserId);

            if (user == null)
            {
                return NotFound(new { message = "Usuário não localizado!" });
                }

            if (user.UserRole == UserRole.User)
            {
                return BadRequest("Somente um usuário administrador pode alterar um filme!");
            }

            movie.MovieUpdateDate = DateTime.Now;

            if (id != movie.MovieId)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            User user = await _context.Users.FindAsync(movie.MovieCreateUserId);

            if (user == null)
            {
                return NotFound(new { message = "Usuário não localizado!" });
            }

            if (user.UserRole == UserRole.User)
            {
                return BadRequest("Somente um usuário administrador pode cadastrar um filme!");
            }

            movie.MovieCreateDate = DateTime.Now;
            movie.MovieUpdateDate = null;

            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.MovieId }, movie);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id, User user_P)
        {

            User user = await _context.Users.FindAsync(user_P.UserId);

            if (user == null)
            {
                return NotFound(new { message = "Usuário não localizado!" });
            }

            if (user.UserRole == UserRole.User)
            {
                return BadRequest("Somente um usuário administrador pode excluir um filme!");
            }

            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
