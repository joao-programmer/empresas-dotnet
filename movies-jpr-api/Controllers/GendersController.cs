﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ioasys_filmes_jrp_api.Models;
using Microsoft.AspNetCore.Authorization;

namespace movies_jpr_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GendersController : ControllerBase
    {
        private readonly DbContext _context;

        public GendersController(DbContext context)
        {
            _context = context;
        }

        // GET: api/Genders
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Gender>>> GetGender()
        {
            return await _context.Genders.ToListAsync();
        }

        // GET: api/Genders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Gender>> GetGender(int id)
        {
            var gender = await _context.Genders.FindAsync(id);

            if (gender == null)
            {
                return NotFound();
            }

            return gender;
        }

        // PUT: api/Genders/5       
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGender(int id, Gender gender)
        {
            User user = await _context.Users.FindAsync(gender.GenderUpdateUserId);

            if (user == null)
            {
                return NotFound(new { message = "Usuário não localizado!" });
            }

            if (user.UserRole == UserRole.User)
            {
                return BadRequest("Somente um usuário administrador pode alterar um gênero!");
            }

            gender.GenderUpdateDate = DateTime.Now;

            if (id != gender.GenderId)
            {
                return BadRequest();
            }

            _context.Entry(gender).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GenderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Genders        
        [HttpPost]
        public async Task<ActionResult<Gender>> PostGender(Gender gender)
        {
            User user = await _context.Users.FindAsync(gender.GenderCreateUserId);

            if (user == null)
            {
                return NotFound(new { message = "Usuário não localizado!" });
            }

            if (user.UserRole == UserRole.User)
            {
                return BadRequest("Somente um usuário administrador pode cadastrar um gênero!");
            }

            gender.GenderCreateDate = DateTime.Now;
            gender.GenderUpdateDate = null;

            _context.Genders.Add(gender);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGender", new { id = gender.GenderId }, gender);
        }

        // DELETE: api/Genders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGender(int id, User user_P)
        {
            User user = await _context.Users.FindAsync(user_P.UserId);

            if (user == null)
            {
                return NotFound(new { message = "Usuário não localizado!" });
            }

            if (user.UserRole == UserRole.User)
            {
                return BadRequest("Somente um usuário administrador pode excluir um gênero!");
            }

            var gender = await _context.Genders.FindAsync(id);
            if (gender == null)
            {
                return NotFound();
            }

            _context.Genders.Remove(gender);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool GenderExists(int id)
        {
            return _context.Genders.Any(e => e.GenderId == id);
        }
    }
}
